import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'journal',
    component: () => import(/* webpackChunkName: "journal" */ '../views/Journal.vue')
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import('../views/Auth.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
