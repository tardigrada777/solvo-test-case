import Vue from 'vue';
import Vuex from 'vuex';
import { mockSignIn } from '@/utils/api';
import { getJournal, JournalEntry, logAuthorizationTry, logServerAccessTry } from '@/utils/localStorage';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
     isUserAuthenticated: false,
     journal: []
  },
  mutations: {
    setAuthState(state, newAuthState: boolean) {
      state.isUserAuthenticated = newAuthState;
    },
    setJournal(state, journal) {
      state.journal = journal;
    }
  },
  actions: {
    async signIn({ commit } ,data: { [key: string]: any }) {
      logAuthorizationTry();
      const res = await mockSignIn(data);
      logServerAccessTry(res.data);
      commit('setAuthState', res.data.data !== null);
      return res;
    },
    getJournal({ commit }) {
      commit('setJournal', getJournal());
    }
  },
  getters: {
    isUserAuthenticated: ({ isUserAuthenticated }) => isUserAuthenticated,
    journal: ({ journal }) => journal.sort(
      (prev: JournalEntry, current: JournalEntry) => prev.date < current.date ? 1 : -1
    )
  },
});
