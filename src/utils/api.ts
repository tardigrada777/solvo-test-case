export const SUCCESS_MOCK_API_URL = 'https://run.mocky.io/v3/8742cc91-e869-4386-b021-7f73c6b2714c';
export const ERROR_MOCK_API_URL = 'https://run.mocky.io/v3/391d6eeb-9509-4bf5-b8c5-d3762b0dd405';

export const mockSignIn = async (data: { [key: string]: any }) => {
  const requests = [SUCCESS_MOCK_API_URL, ERROR_MOCK_API_URL];
  const randomCall = Math.floor(Math.random() * 2);
  const res = await fetch(requests[randomCall], {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return {
    status: res.status,
    data: await res.json()
  }
}
