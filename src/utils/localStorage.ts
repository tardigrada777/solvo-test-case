const JOURNAL_KEY = 'auth_journal';

export class JournalEntry {
  public constructor(
    public type: string,
    public date: string,
    public data?: object | any
  ) {}
}

export const getJournal = (): Array<JournalEntry> => JSON.parse(localStorage.getItem(JOURNAL_KEY) || '[]');

export const saveJournal = (journal: Array<JournalEntry>) => localStorage.setItem(JOURNAL_KEY, JSON.stringify(journal));

export const clearJournal = () => localStorage.setItem(JOURNAL_KEY, '[]');

export const newJournalEntry = (entry: JournalEntry) => {
  const journal = getJournal();
  journal.push(entry);
  saveJournal(journal);
}

export const logAuthorizationTry = () => {
  const authorizationTry = new JournalEntry('Попытка авторизации', Date.now().toString());
  newJournalEntry(authorizationTry);
}

export const logServerAccessTry = (data: any) => {
  const serverAccessTry = new JournalEntry(
    'Обращение к серверу',
    Date.now().toString(),
    data
  );
  newJournalEntry(serverAccessTry);
}